Vorschlag Entwurf:

- Zeit zu einem Enum machen (drei Werte)
- Prüfen was Collider handler von Unity bezüglich der VehicleController schon von kann.
- Für Vererbung von MonoBehaviour die Klassen vllt einfärben. (Rot für MonoBehaviour, Blau für eine andere Klasse)
- Visitor Pattern in der Ampelschalten (Meiner war skeptisch)
  - Ampel vllt mit Plugin umsetzen
- Nochmal bitte Fahrräder einbauen (sollte wir wohl machen)

Wunsch Generell:

- Tutorial modus um den Nutzenden in die Nutzung von VR einzuführen (super simple)
