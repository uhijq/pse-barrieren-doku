Dieses Projekt enthält die Dokumentation zu dem PSE Projekt: Bahnhofsimulation der Bahnstation "Durlacher Tor/ KIT Süd".

## Wiki 

Großteils der notizen sind im [Wiki](../../wikis/home) des Projektes zu finden.

## Pflichtenheft

Das Pflichtenheft wird mit Latex geschrieben. Das Latex Dokument wird mit Overleaf verfasst und ist [hier](https://www.overleaf.com/read/qrcfyhdjbgtw#742bdf) zu finden.
