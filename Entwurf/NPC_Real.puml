﻿@startuml
class SceneConfiguration #peachpuff {
    - int: valueNCPs
    - int: valueCars
    - bool: leaded
    - int: time
    +SetAll(valueNPCs:int, valueCars:int, leaded:boolean, time:int):void
}

class PedestrianCollisionDetector #peachpuff{
    - pedestrians : List<IVehicleObserver>
    + AddPedestrian(pedestrian:IVehicleObserver) : void
    + RemovePedestrian(pedestrian:IVehicleObserver) : void
    + NotifyPedestrians() : void
}

class Platform #peachpuff {
    - displays : List<TextMeshPro>
    - _hours : int
    - _minutes : int
    - _occupied : bool
    - _stopTime : int
    - _schedule : Schedule
    - _pCollider : PedestrianCollisionDetector
    - _vCollider : VehicleCollisionDetector
    - _timeManager : TimeManager
    - Awake() : void
    - Start() : void
    - OnEnable() : void
    - OnDisable() : void
	- BuildDisplayText() : string
    - GetAmountOfWhitespaces(amount:int) : string
    - GetWaitingTime(hour:int, minute:int) : int
    + UpdateSchedule() : void
    + SetStopTime(stopTime:int) : void
    + GetStopTime() : int
    + UpdateTextOnDisplay() : void
    + UpdateTime(hours:int, minutes:int) : void
    + IsOccupied() : bool
    + SetOccupied(occupied:bool) : void
}

interface IVehicleObserver #peachpuff {
    +VehicleEntered() : void
    +VehicleLeft() : void
}


class TrafficLight #peachpuff{
    - objRenderer : Renderer
    - observer : List<ITraversableObserver>
    - traversable : bool
    - yellowOn : bool
    - redOn : bool
    - greenOn : bool
    - requested : bool
    - shouldPlaySound : bool = false
    - greenOffset : float
    - greenToYellowOffset : float
    - redToYellowOffset : float
    - redOffset : float
    - trafficLightGroups : List<int>
    - myTrafficCoordinator : TrafficLightCoordinator
    + m_MyAudioSourceGreenBeeping : AudioSource
    - Awake() : void
    - Start() : void
    - NotifyCoordinator() : void
    + TraversableRequested() : void
    + SetGreen() : void
    + SetYellow() : void
    + SetRed() : void
    + IsRequested() : bool
    + IsTraversable() : bool
    + SetTraversable(traversable:bool) : void
    + NotifyTraversableChanged() : void
    + AddTraversableObserver(observer:ITraversableObserver) : void
    + RemoveTraversableObserver(observer:ITraversableObserver) : void
}

interface ITraversableObserver {
    OnTraversableChanged() : void
}

class NavMeshAgent #lightblue{
    - velocity : Vector3
}

hide empty members
skinparam linetype ortho

package AgentsControl{

    package Agents{
    
        abstract class "AgentController" as AC #f7bfc2 {
            - moving : bool
            - currentDestination : Vector3
            - currentTrafficLight : TrafficLight
            - animator : Animator
            - lastAnimation : string
            - destinationDirty : bool
            - agentManager: AgentManager
            
            - PreventAgentMovement() : void
            - TryRegisterAtTrafficLight(other:Collider) : void
            - GetBestSpeedAnimation(speed:float) : string
            - RegisterAtTrafficLight(trafficLight:TrafficLight) : void
            - UnregisterAtTrafficLight(trafficLight:TrafficLight) : void
            # CanCrossTrafficLight(trafficLight:TrafficLight) : bool
            # {abstract} IsTrafficLight(other:Collider) : bool
            # OnTriggerEnter(other:Collider) : void
            + OnTraversableChanged() : void
            - Awake() : void
            + Update() : void
        }
        
        AC "1" *-- "n" AgentsControl.SpeedAnimation
        
        ITraversableObserver <|-- AC
        AC --> TrafficLight : readStatus >
        
        AC "agent  1 " -u-> "1" NavMeshAgent : controls >
        
        class "Pedestrian" as Ped extends AC{
            - static TRAFFIC_LIGHT_TAG : string
            - isEnteringVehicle : bool
            - currentPlatformCollider : PedestrianCollisionDetector
            - isAtPlatform : bool
            - destinationPlatform : Platform
            
            - CheckPlatformStatus(platform : Platform) : void
            - RemoveAgent() : void
            - ShouldDespawnAtDestination() : bool
            - TryRegisterAtPlatform(other:Collider) : void
            - EnterVehicle() : void
            # IsTrafficLight(other:Collider) : bool
            # OnTriggerEnter(other:Collider) : void
            + Initialize(agentManager:AgentManager, destination:Vector3, destinationPlatform:Platform) : void
            + Update() : void
            + VehicleEntered() : void
            + VehicleLeft() : void
        }
        Ped ..|> IVehicleObserver
        Ped --> PedestrianCollisionDetector : currentPlatformCollider >
        Ped --> Platform : readStatus >
        
        class "Bicycle" as Bike extends AC{
            - <<const>> TRAFFIC_LIGHT_TAG : string
            # IsTrafficLight(other:Collider) : bool
        }
    
    }
    
    class Region {
        - center : Vector3
        - width : float
        - depth : float
        - rotation : Vector3
        + GetRandomLocation() : Vector3
    }
    
    class PlatformWithRegion {
        - platform : Platform
        - region : Region
    }
    
    PlatformWithRegion --> Region
    PlatformWithRegion --> Platform
        
        
    class "AgentManager" as AM #f7bfc2{
        - pedestrianPrefabs : List<GameObject>
        - bicyclePrefabs : List<GameObject>
        - bicycleRatio : float
        
        - ShouldSpawnNewPedestrian() : bool
        - ShouldSpawnNewBicycle() : bool
        - GetNextPedestrianPrefab() : GameObject
        - GetNextBicyclePrefab() : GameObject
        - GetDestinationPlatform() : PlatformWithRegion
        - GetRandomEdgeRegion() : Region
        - Awake() : void
        - StartCoroutines() : void
        - OnEnable() : void
        - OnDisable() : void
        - SpawnNPCsCoroutine() : IEnumerator
        + SpawnNewBicycle() : void
        - SpawnNewPlatformPedestrian() : void
        - SpawnNewRegionPedestrian() : void
        + SpawnNewPedestrian() : void
        + NotifyBicycleRemoved(bicycle:Bicycle) : void
        + NotifyPedestrianRemoved(pedestrian:Pedestrian) : void
    }
    
    AM "1" -- "n" Ped : currentPedestrians >
    AM "1" --- "n" Bike: currentBicycles >
    
    AM -u-> SceneConfiguration : reads valueNPCs >
    
    AM "1" o--> "n" PlatformWithRegion: platforms >
        
    AM "1" o--> "n" Region : edgeRegions >
    
    
    class SpeedAnimation {
        + MinSpeed : float
        + Animation : string
    }
}
@enduml
