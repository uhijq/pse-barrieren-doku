﻿@startuml
'https : //plantuml.com/class-diagram
'peachpuff color is for classes from another class diagram
    
class SceneConfiguration #peachpuff

interface IPlatformObserver #peachpuff {
    +LeavePlatform() : void
}
class VehicleCollisionDetector #peachpuff {
    -vehicle : IPlatformObserver
    
    +AddVehicle(vehicle : IPlatformObserver) : void
    +RemoveVehicle(vehicle : IPlatformObserver) : void
    +NotifyVehicle() : void
}
class Platform #peachpuff {
        -occupied : bool
        -stopTime : int
        -schedule : Schedule
        -pCollider : PedestrianCollisionDetector
        -vCollider: VehicleCollisionDetector
    
        +UpdateSchedule() : void
        +SetStopTime(stopTime: int) : void
    }

interface ICarIntersectionObserver #peachpuff{
    + UpdateWaitingCar() : void
}
interface ITramIntersectionObserver #peachpuff{
    + UpdateWaitingTrams() : void
} 

class CarLight #peachpuff {
    - carsWaitingForGreenList : List<ICarIntersectionObserver>
    + StartNotifingCar(o : ICarIntersectionObserver) : void
    + StopNotifingCar(o : ICarIntersectionObserver) : void
    + SetGreen() : void
    + SetYellow() : void
    + SetRed() : void
    + NotifyIsTraversable() : void
}

class TramLight #peachpuff{
    - tramsWaitingForGreenList : List<ITramIntersectionObserver>
    + StartNotifingTram(o : ITramIntersectionObserver) : void
    + StopNotifingTram(o : ITramIntersectionObserver) : void
    + SetGreen() : void
    + SetYellow() : void
    + SetRed(): void
    + NotifyIsTraversable(): void
}
class PublicTransportDescriptor #peachpuff {
    -direction : String
    -line : int
    -waitingTime : int
    
    +UpdateWaitingTime(amount : int) : void
}

class Waypoint #lightblue {
    + allowedCars : List<VehicleTypes>
    + maxSpeed : int
    + giveWay : bool
    + enter : bool
    + exit : bool
    + stop : bool
    - associatedIntersection : IIntersection
    + SetIntersection(intersection : IIntersection, giveWay : bool, stop : bool, enter : bool, exit : bool) : void
    + CanChange() : bool
    + IsInIntersection() : bool
}

class TrafficComponent #lightblue extends MonoBehaviour  {  
    + player : Transform  
    + nrOfVehicles : int
    + vehiclePool : VehiclePool  
    + minDistanceToAdd : float
    + distanceToRemove : float
    + yellowLightTime : float  
    + greenLightTime : float  
}

hide empty members
remove MonoBehaviour
skinparam linetype ortho

class MonoBehaviour #lightblue
package TrafficControl {
    package Model{
        class TrackModel{
            	+ GetPositionAndSpeedOfProgress(progress : Double, out position : Vector3, out direction : Vector3) : void
            }
            note right of TrackModel::GetPositionAndSpeedOfProgress
            Writes to the given progress and position objects
            end note
            
            class TrackPoint{
            	- position : Vector3
            	- controlPoint : Vector3
            }
            
            TrackModel "1" *--> "n" TrackPoint : is described by >
    }
    
    abstract class VehicleConfigurator{
    	- prefab : GameObject
    	+ {abstract} ConfigureVehicle(vehicle : GameObject) : void
    }
    
    class BusAndTramConfigurator{
    	- scheduling : PublicTransportDescriptor
    	+ <<constructor>> BusAndTramConfigurator(prefab : GameObject, publicTransportDescriptor : PublicTransportDescriptor)
    	+ ConfigureVehicle(vehicle : GameObject) : void
    }
    
    BusAndTramConfigurator -u-|> VehicleConfigurator
    
    BusAndTramConfigurator ..> TrafficControl.Vehicles.IPublicTransportController
    
    
    package Vehicles{
    note "Handles collision prevention with obstacles \n\
        and interactions with traffic lights" as collisionNote
        collisionNote .. VehicleController
        abstract class VehicleController #f7bfc2 {
            - obstacles : List<Collider>
            - trafficLight : TrafficLight
            - moving : bool
        	- trackProgress : double
        	- currentSpeed : double
        	- acceleration : double
        	# {abstract} RegisterAtTrafficLight(trafficLight : TrafficLight) : bool
        	+ Update() : void
        	+ OnTriggerEnter(collider : Collider) : void
        	+ OnTriggerExit(collider : Collider) : void
        	+ HandleTrigger(collider : Collider) : bool
            + HandleTriggerExit(collider : Collider) : bool
        }
        
        ' note "used to expand the functionallity" as expandNote
        ' expandNote .. VehicleController::handleCollision
        ' expandNote .. VehicleController::handleTrigger
        
        VehicleController "1" --> "1" TrafficControl.Tracks.VehicleTrack : follows >
        
        interface IPublicTransportController {
            + SetLine(line : Integer) : void
            + SetDirection(direction : String) : void
        }
        
        class TramController extends VehicleController{
            - currentPlatform : VehicleCollisionDetector
            - isWaitingAtPlatform : bool
            # RegisterAtTrafficLight(trafficLight : TrafficLight) : bool
            + UpdateWaitingTrams() : void
            + HandleTrigger(collider : Collider) : bool
        	+ SetLine(line : Integer) : void
            + SetDirection(direction : String) : void
            + LeavePlatform() : void
        }
        TramController ..|> IPlatformObserver
        TramController .u.|> IPublicTransportController
        TramController .u.|> ITramIntersectionObserver
        TramController "1" --> "1" Platform : currentPlatform
        TramController --> TramLight : registers at light >
        TramController "1" --> "1" VehicleCollisionDetector
        
        class BusController {
            - currentPlatform : VehicleCollisionDetector
            - isWaitingAtPlatform : bool
            # RegisterAtTrafficLight(trafficLight : TrafficLight) : bool
        	+ UpdateWaitingCar() : void
            + HandleTrigger(collider : Collider) : bool
        	+ SetLine(line : Integer) : void
            + SetDirection(direction : String) : void
            + LeavePlatform() : void
        }
        BusController ..|> IPlatformObserver
        BusController ..|> IPublicTransportController
        BusController .u.|> ICarIntersectionObserver
        BusController -u-|> VehicleController
        BusController --> VehicleCollisionDetector
        
        BusController "1" --> "1" Platform : currentPlatform 
        
        BusController --> CarLight : registers at light >
    }
    
    package Tracks{
        class VehicleTrack{
                - trackModel : TrackModel
                - vehiclesOnTrack : Integer
                - vehicles : List<VehicleController>
                + SpawnVehicle(configurator : VehicleConfigurator) : bool
                + VehicleDidLeaveTrack(vehicle : VehicleController) : void
            }
            VehicleTrack "1" o---> "1" TrackModel : manages >
            
            class ScheduledVehicleSpawner #f7bfc2 {
            	- schedule : Schedule
            	- prefab : GameObject
            	+ Update() : void
            }
            
            ScheduledVehicleSpawner ..> PublicTransportDescriptor
            ScheduledVehicleSpawner ..> BusAndTramConfigurator
            
            BusAndTramConfigurator "n" --> "1" PublicTransportDescriptor : uses information >
            
            ScheduledVehicleSpawner "1" --> "1" VehicleTrack : spawns on >
    }
    
    
    
    class TrafficSystemConfigurator #f7bfc2{
    	+ Awake() : void
    }
    
    SceneConfiguration <-- TrafficSystemConfigurator : reads >
    
    TrafficSystemConfigurator --> TrafficComponent : manipulates >
    
    
    note "Sets the number of vehicles of the TrafficComponent" as traffic_note
    
    traffic_note .. TrafficSystemConfigurator::Awake
    
    
    class TrafficLightAdapter{
        + UpdateWaitingCar() : void
    }
    
    TrafficLightAdapter .u.|> ICarIntersectionObserver
    
    TrafficLightAdapter --> Waypoint : changes stop status >
    TrafficLightAdapter --> CarLight : readStatus >
}



@enduml